<?php

header('Content-Type: text/html; charset=utf-8');

require_once('../src/class/isValid.php');
require_once ('vendor/autoload.php');

class isValidTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function testEmail()
    {
        //$test = new masterdan\isValid();
        $test = new isValid();
        $this->debugEnabled = true;
        $this->assertTrue($test->email('net@emaila.ru'));
        $this->assertFalse($test->email('netemailaru'));
        $this->assertFalse($test->email('net@emaila.'));
        $this->assertFalse($test->email('netz'));
        $this->assertFalse($test->email('дяляля'));
        $this->assertFalse($test->email('12345'));
        $this->assertFalse($test->email('^^^netemailaru'));
    }
}
