<?php

//namespace masterdan;

//прописываем везде имя класса - заменяем TESTCLASSNAME на имя класса
//прописываем везде имя метода - заменяем TESTMETHODNAME на имя метода

header('Content-Type: text/html; charset=utf-8');

require_once('../src/interface/TESTCLASSNAME.php');
require_once('../src/class/TESTCLASSNAME.php');

require_once ('vendor/autoload.php');

class TESTCLASSNAMETest extends \PHPUnit\Framework\TestCase
{
    public $test;
    public function __construct()
    {
        parent::__construct();
        $this->test = new TESTCLASSNAME();
    }
    public $testClass;

    public function setUp()
    {
        if (property_exists($this->testClass, 'debugEnabled'))
            $this->testClass->debugEnabled = true;
    }

    public function testTESTMETHODNAME()
    {
        //$this->assertTrue($this->test->TESTMETHODNAME('net@emaila.ru'));
        //$this->assertFalse($this->test->TESTMETHODNAME('netemailaru'));
    }

    public function tearDown()
    {
        unset($this->testClass);
    }
}
