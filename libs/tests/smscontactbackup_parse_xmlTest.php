<?php

header('Content-Type: text/html; charset=utf-8');

use smscontactbackup\smscontactbackup_parse_xml;

require_once('../src/class/smscontactbackup_parse_xml.php');
require_once ('vendor/autoload.php');

class smscontactbackup_parse_xmlTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function TestRaschetaZvonkov()
    {
        //тестируем без учёта периода на тестовых данных
        $test = new smscontactbackup_parse_xml(false, '../data/xml_file/calllogs_test.xml');
        $this->assertEquals(5, $test->zvonki_neponyatnih_zvonkov_chislo, 'Kolichestvo neponyatnih zvonkov ne sovpalo bez ucheta perioda');
        $this->assertEquals(0, $test->zvonki_neponyatnih_zvonkov_secund, 'Kolichestvo sekund neponyatnih zvonkov ne sovpalo bez ucheta perioda');
        $this->assertEquals(292, $test->zvonki_ishodyashih_zvonkov_chislo, 'Kolichestvo ishodyashih zvonkov ne sovpalo bez ucheta perioda');
        $this->assertEquals(731, $test->zvonki_ishodyashih_zvonkov_minut, 'Kolichestvo minut ishodyashih zvonkov ne sovpalo bez ucheta perioda');
        $this->assertEquals(322, $test->zvonki_vhodyashih_zvonkov_chislo, 'Kolichestvo vhodyashih zvonkov ne sovpalo bez ucheta perioda');
        $this->assertEquals(2775, $test->zvonki_vhodyashih_zvonkov_minut, 'Kolichestvo minut vhodyashih zvonkov ne sovpalo bez ucheta perioda');
        $this->assertEquals("1638515635", $test->rezultat_obrabotki[615]['time'], 'Vremya zvonka 615 zapisi ne sovpalo bez ucheta perioda');
        $this->assertEquals("+79119240875", $test->rezultat_obrabotki[615]['phone'], 'Telefon 615 zapisi ne sovpal bez ucheta perioda');
        $this->assertEquals(619, count($test->rezultat_obrabotki), 'Kolichestvo zapisey v massive ne sovpalo bez ucheta perioda');
        //$this->assertEquals(0, 0);
        //$this->assertTrue( == 619));
        unset($test);

        //теперь тестируем с учётом периода на тестовых данных
        $test = new smscontactbackup_parse_xml(true, '../data/xml_file/calllogs_test.xml');
        $this->assertEquals(0, $test->zvonki_neponyatnih_zvonkov_secund, 'Kolichestvo sekund neponyatnih zvonkov ne sovpalo s uchetom perioda');
        $this->assertEquals("1638515635", $test->rezultat_obrabotki[615]['time'], 'Vremya zvonka 615 zapisi ne sovpalo s uchetom perioda');
        $this->assertEquals("+79119240875", $test->rezultat_obrabotki[615]['phone'], 'Telefon 615 zapisi ne sovpal s uchetom perioda');
        $this->assertEquals(619, count($test->rezultat_obrabotki), 'Kolichestvo zapisey v massive ne sovpalo s uchetom perioda');
        //$this->assertEquals(0, 0);
        //$this->assertTrue( == 619));
        unset($test);
    }
}

