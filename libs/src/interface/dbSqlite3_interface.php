<?php

//namespace masterdan;

/**
 * interface dbSqlite3
 * задаёт заготовку для класса базы
 */
interface dbSqlite3_interface
{

    /**
     * dbSqlite3 constructor - определяем, стоит ли глобальный флаг дебага.
     * @param string $db_filename - файл с базой
     * если да, то присваиваем локальному флагу значение
     */
    public function __construct(string $db_filename);

    /**
     * вохвращает объект подключения к базе
     * @return dbSqlite3_interface - объект обращения к базе
     */
    //public static function get_db_handler ():dbSqlite3_main;

    /**
     * функция подключения файла базы
     * использует string $this->db_file - путь к файлу с базой
     * @return bool - результат (true-подключились, false-не подключились)
     */
    public function connect ():bool;
    public function disconnect ():bool;

    /**
     * выполнение запроса к базе
     * @param string $query - текст запроса
     * @return array - массив значений результата
     */
    public function query (string $query):array;

    /**
     * выполнение запроса к базе
     * @param string $query - текст запроса
     * @param string $sub - строка, которую заменять на переменные. Лучше использовать непопулярный текст
     * @param array $params - массив значений для подстановки
     * @return array - массив значений результата
     */
    public function query_stmt (string $query, string $sub, array $params):array;
};
