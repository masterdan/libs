<?php

//namespace masterdan;

/**
 * Class isValid
 * проверяет правильность данных
 */
class isValid
{
    /**
     * @var bool $debugEnabled - флаг включенной отладки
     * при его установке в true будут выводиться дополнительные сообщения
     */
    public $debugEnabled = false;

    /**
     * isValid constructor - определяем, стоит ли глобальный флаг дебага.
     * если да, то присваиваем локальному флагу значение
     */
    public function __construct ()
    {
        if (isset($GLOBALS['flag_debug_enabled']))
            $this->debugEnabled = true;
    }

    /**
     * функция проверки правильности e-mail'a
     * @param string $email - входной адрес почты
     * @return bool - результат (true-верный, false-неверный)
     */
    //public function email ( string $email ):bool
    public function email ( string $email )
    {
        $rezult = false;
        $rezult1 = false;
        $rezult2 = false;
        /**
         * @link https://www.php.net/manual/ru/filter.examples.validation.php
         */
        if (function_exists('filter_var'))
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                if ($this->debugEnabled)
                    print "e-mail не прошёл первую проверку<br>";
                return(false);
            }
            else
                print "e-mail прошёл первую проверку успешно<br>";

        /*        else
                    if ($this->debugEnabled)
                        print "функция первой проверки не существует-пропускаем первую проверку<br>";
        */


        if (function_exists('preg_match'))
            if (preg_match("/[^(\w)|(\@)|(\.)|(\-)]/", $email))
            {
                if ($this->debugEnabled)
                    print "e-mail не прошёл вторую проверку<br>";
                return(false);
            }
            else
                if ($this->debugEnabled)
                    print "e-mail прошёл вторую проверку успешно<br>";
        return (true);
    }
};