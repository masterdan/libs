<?php

//namespace masterdan;

require_once ("../interface/dbSqlite3_interface.php");

/**
 * Class dbSqlite3
 * обёртка к запросам в базу
 * поддерживает подготовленные запросы и обычные
 * использовать через
 *  $db = new dbSqlite3(DB_FILE);
 *  $params[] = $form_email;
 *  $sql = "select user.id, user.email, user_info.sname, user_info.name from user,user_info where user.email like ZZZ and user.id = user_info.user_id;";
 *  $sql_result = $db->query_stmt($sql, 'ZZZ', $params);
 *  $db->disconnect();

 */
class dbSqlite3 implements dbSqlite3_interface
{
    /**
     * @var bool $debugEnabled - флаг включенной отладки
     * при его установке в true будут выводиться дополнительные сообщения
     */
    public $debugEnabled = false;

    /**
     * @var string $db_file - имя файла с базой
     */
    public $db_file;

    /**
     * @var dbSqlite3_main $db_handler - объект базы
     */
    private $db_handler;

    /**
     * @var SQLite3 $db - объект базы sqlite3
     */
    private $db;

    /**
     * dbSqlite3 constructor - определяем, стоит ли глобальный флаг дебага.
     * если да, то присваиваем локальному флагу значение
     */
    public function __construct(string $db_file)
    {
        if (isset($GLOBALS['flag_debug_enabled']))
            $this->debugEnabled = true;
        $this->db_file = $db_file;
    }

    /**
     * проверка файла базы на доступность
     * использует string $this->db_file - путь к файлу с базой
     * @return bool - результат (true-файл доступен, false-что-то не удалось)
     */
    private function db_file_check():bool
    {
        $rezult = true;

        if (!file_exists($this->db_file))
        {
            if ($this->debugEnabled)
                print "файл базы данных не существует<br>";
            return(false);
        }
        elseif ($this->debugEnabled)
            print "файл базы данных существует<br>";

        if (!is_writable($this->db_file))
        {
            if ($this->debugEnabled)
                print "файл базы данных не доступен для записи<br>";
            return(false);
        }
        elseif ($this->debugEnabled)
            print "файл базы данных доступен для записи<br>";
        return($rezult);
    }

    /**
     * функция подключения файла базы
     * использует string $this->db_file - путь к файлу с базой
     * @return bool - результат (true-подключились, false-не подключились)
     */
    public function connect():bool
    {
        if ($this->db_file_check())
            $this->db = new SQLite3($this->db_file);
        if (!$this->db)
        {
            if ($this->debugEnabled)
                print "создать подключение к файлу не удалось<br>";
            return (false);
        }
        else
            return(true);
    }

    public function disconnect():bool
    {
        if ($this->db)
        {
            $this->db->close();
            unset($this->db);
            return(true);
        }
        return(false);
    }

    /**
     * получает объект с результатами выборки и выгружает их в массив
     * @param object $sqlite_rezult - результаты выборки запроса
     * @return array - массив значений результата
     */
    public function result_sqlite3_to_array(object $sqlite_rezult):array
    {
        $rezult = null;
        while ($row = $sqlite_rezult->fetchArray())
            $rezult[] = $row;
        if ($this->debugEnabled)
        {
            print "массив ответа получился такой<br>";
            var_dump($rezult);
            print "<br>";
        }
        if (!$rezult)
            return([]);
        return($rezult);
    }

    /**
     * выполнение запроса к базе
     * @param string $query - текст запроса
     * @return array - массив значений результата
     */
    public function query(string $query):array
    {
        if (!$this->db)
            $this->connect();
        $query_rezult = [];
        $time = date("Y-m-d H:m:s");;
        $query = str_replace("CURTIME()", "$time", $query);
        //$query = \SQLite3::escapeString($query);
        if ($this->debugEnabled)
            print "запрос в базу получился $query<br>";
        if ((substr_count($query, "insert")) or (substr_count($query, "delete")) or (substr_count($query, "update")) or (substr_count($query, "INSERT")) or (substr_count($query, "DELETE")) or (substr_count($query, "UPDATE")))
            $this->db->exec($query);
        else
            $query_rezult = $this->db->query($query);
        if (empty($query_rezult))
            return($query_rezult);
        else
            return($this->result_sqlite3_to_array($query_rezult));
    }

    /**
     * выполнение запроса к базе
     * @param string $query - текст запроса
     * @param string $sub - строка, которую заменять на переменные. Лучше использовать непопулярный текст
     * @param array $params - массив значений для подстановки
     * @return array - массив значений результата
     */
    public function query_stmt(string $query, string $sub, array $params):array
    {
        if (!$this->db)
            $this->connect();
        $time = date("Y-m-d H:m:s");;
        $query = str_replace("CURTIME()", "$time", $query);
        $query = \SQLite3::escapeString($query);

        /**
         * @link https://www.php.net/manual/en/sqlite3stmt.bindvalue.php
         */
        //проходим по массиву и делаем готовый sql-запрос и привязываем значения к полученным
        //$sql_params = null;
        $sql_params_count = 0;
        $query_result = $query;
        foreach ($params as $param)
        {
            $sql_params = ":$sql_params_count";
            $query_result = $this->string_replace_once($sub,$sql_params,$query_result);
            $sql_params_count++;
        };

        if ($this->debugEnabled)
        {
            print "строка запроса была изначально $query<br>стала:<br>$query_result<br>";
            print "подготовленный запрос в базу получился $query_result<br>";
        }
        $stmt = $this->db->prepare($query_result);

        $sql_params_count = 0;
        foreach ($params as $param)
        {
            $sql_params = ":$sql_params_count";
            $stmt->bindValue($sql_params, $param);
            $query_result = str_replace($sql_params, $param, $query_result);
            if ($this->debugEnabled)
                print "для параметра $sql_params ставим значение $param<br>";
            $sql_params_count++;
        };
        $sql_result = $stmt->execute();

        if ($this->debugEnabled)
            print "итоговый запрос в базу получился $query_result<br>";

        return($this->result_sqlite3_to_array($sql_result));
    }

    /**
     * однократная замена подстроки
     * @param string $search - искомая строка
     * @param string $replace - на что заменить
     * @param string $string - текст, в котором это сделать
     * @return string - готовый результат
     */
    public function string_replace_once(string $search, string $replace, string $string):string
    {
        $pos = strpos($string, $search);
        return $pos!==false ? substr_replace($string, $replace, $pos, strlen($search)) : $string;
    }
};
