<?php
declare(strict_types=1);

namespace smscontactbackup;

/*
 * класс делает статистику входящих и исходящих звонков
 * из файла smscontactsbackup с телефона.
 * Считает количество и продолжительность входящих и исходящих звонков,
 * выбирая период за прошлый месяц.
 * Принимает файл со звонками в виде массива
 * или можно положить в файл data/xml_file/calllogs.xml
 */


class smscontactbackup_parse_xml
{
    public $xml_massiv;

    /**
     * @var array
     */
    public $rezultat_obrabotki;
    /**
     * @var string
     */
    public $xml_filename = '../data/xml_file/calllogs.xml';

    public $zvonki_vhodyashih_zvonkov_chislo = 0;
    public $zvonki_vhodyashih_zvonkov_secund = 0;
    public $zvonki_vhodyashih_zvonkov_minut = 0;
    public $zvonki_ishodyashih_zvonkov_chislo = 0;
    public $zvonki_ishodyashih_zvonkov_secund = 0;
    public $zvonki_ishodyashih_zvonkov_minut = 0;
    public $zvonki_neponyatnih_zvonkov_chislo = 0;
    public $zvonki_neponyatnih_zvonkov_secund = 0;
    public $schitat_tolko_proshliy_mesyac = true;

    public function __construct ($schitat_tolko_proshliy_mesyac = true, $file = '')
    {
        $this->schitat_tolko_proshliy_mesyac = $schitat_tolko_proshliy_mesyac;
        if (!empty($file))
            $this->xml_filename = $file;
        //elseif (empty($this->xml_text))
        $this->xml_massiv = $this->prochitat_xml_iz_fayla();
        $this->xml_v_massiv();

        $this->rezultat_obrabotki = $this->ubiraem_iz_massiva_lishnee();
        //var_dump($this->rezultat_obrabotki);
        //return ($this->rezultat_obrabotki);
        $this->podschet_dannih_zvonkov();
        $this->zvonki_vhodyashih_zvonkov_minut = (int) ($this->zvonki_vhodyashih_zvonkov_secund / 60);
        $this->zvonki_ishodyashih_zvonkov_minut = (int) ($this->zvonki_ishodyashih_zvonkov_secund / 60);
        print "Zvonkov vhodyashih " . $this->zvonki_vhodyashih_zvonkov_chislo . " na " . $this->zvonki_vhodyashih_zvonkov_minut . " minut \r\n";
        print "Zvonkov ishodyashih " . $this->zvonki_ishodyashih_zvonkov_chislo . " na " . $this->zvonki_ishodyashih_zvonkov_minut . " minut\r\n";
        print "Zvonkov neponyatnih " . $this->zvonki_neponyatnih_zvonkov_chislo . " na " . $this->zvonki_neponyatnih_zvonkov_secund . " secund\r\n";
    }

    private function xml_v_massiv ()
    {
        foreach ($this->xml_massiv as $line_num => $line)
            $this->rezultat_obrabotki[] = explode ('"', $line);
    }

    public function prochitat_xml_iz_fayla() :array
    {
        //читаем данные из файла
        $massiv_s_dannimi = [];
        if (file_exists($this->xml_filename) and is_readable($this->xml_filename))
        {
            $lines = file($this->xml_filename);
            foreach ($lines as $line_num => $line)
            {
                //читаем построчно файл
                if (substr_count($line, 'name=""'))
                    $line = str_replace ('name=""', 'name="unnamed"', $line);
                if (substr_count($line, '<log number="'))
                    $massiv_s_dannimi[] = $line;
            }
        }
        else
        {
            print "File " . $this->xml_filename . " ne nayden-ne mogu prochest ishodnie dannie ;-(";
            die;
        }
        print "Sobrano " . count($massiv_s_dannimi) . " strok v massiv.\r\n";
        var_dump($massiv_s_dannimi[14]);
        return $massiv_s_dannimi;
    }

    private function ubiraem_iz_massiva_lishnee() :array
    {
        //чистим массив от мусорных данных
        $massiv_itogoviy = [];
        foreach ($this->rezultat_obrabotki as $stroka_1 => $stroka)
        {
            $massiv_itogoviy[] = [
                'phone' => $stroka[1],
                'date' => $stroka[3],
                'time' => substr($stroka[5], 0, 10),
                'type' => $stroka[7],
                'seconds' => (int) $stroka[13]
                ];
        }
        return ($massiv_itogoviy);
    }

    private function podschet_dannih_zvonkov()
    {
        //считаем данные по звонкам - количество входящих, исходящих
        //и продолжительность входящих и исходящих

        //если указан расчёт только за этот месяц
        if ($this->schitat_tolko_proshliy_mesyac == true)
            $period = $this->opredelayem_time_proshlogo_mesyaca();
        else
        {
            //указан расчёт всех звонков, независимо от периода
            $period['nachalo_proshlogo_mesyaca'] = 0;
            $period['nachalo_etogo_mesyaca'] = 99999999999;
        }

        $count = 0;
        foreach ($this->rezultat_obrabotki as $stroka_1 => $stroka)
        {
            $count ++;
            print "count = " . $count . " , time=" . $stroka['time'] . "\r\n";
            //проверяем, попадает ли дата в период прошлого месяца
            if ((int) $stroka['time'] < $period['nachalo_proshlogo_mesyaca'])
            {
                print "Vremya " . $stroka['time'] . " ne prodoshlo po periodu - menshe " . $period['nachalo_proshlogo_mesyaca'] . " -propuskaem.\r\n";
                continue;
            }
            if (((int) $stroka['time'] > $period['nachalo_etogo_mesyaca']))
            {
                print "Vremya " . $stroka['time'] . " ne prodoshlo po periodu - bolshe " . $period['nachalo_etogo_mesyaca'] . " -propuskaem.\r\n";
                continue;
            }

            if ($stroka['type'] == '1')
            {
                //входящий звонок
                $this->zvonki_vhodyashih_zvonkov_chislo ++;
                $this->zvonki_vhodyashih_zvonkov_secund = $this->zvonki_vhodyashih_zvonkov_secund + (int) $stroka['seconds'];
                print "Vhodyashiy zvonok na " . $stroka['seconds'] . " secund\r\n";
            }
            elseif ($stroka['type'] == '2' or $stroka['type'] == '3')
            {
                //исходящий звонок
                $this->zvonki_ishodyashih_zvonkov_chislo ++;
                $this->zvonki_ishodyashih_zvonkov_secund = $this->zvonki_ishodyashih_zvonkov_secund + (int) $stroka['seconds'];
                print "Ishodyashiy zvonok na " . $stroka['seconds'] . " secund\r\n";
            }
            else
            {
                //непонятный звонок
                $this->zvonki_neponyatnih_zvonkov_chislo ++;
                $this->zvonki_neponyatnih_zvonkov_secund = $this->zvonki_neponyatnih_zvonkov_secund + (int) $stroka['seconds'];
                print "Neizvestniy zvonok na " . $stroka['seconds'] . " secund\r\n";
            }
        }
    }

    private function opredelayem_time_proshlogo_mesyaca()
    {
        $period['nachalo_proshlogo_mesyaca'] = strtotime(date("d.m.Y",strtotime("first day of previous month"))); // первый день прошлого месяца
        $period['nachalo_etogo_mesyaca'] = strtotime(date("d.m.Y",strtotime("first day of this month")));
        //date("d.m.Y",strtotime("last day of previous month"));  // последний день прошлого месяца
        //TODO определяем месяц, за который надо посчтить звонки-на 1 меньше текущего
        var_dump($period);
        //sleep(100);
        return($period);
    }
}
