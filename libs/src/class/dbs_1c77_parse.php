<?php

namespace dbf_1c77;

/*
 * класс пробует парсить dbf-базу от 1С 7.7
 */


class dbf_1c77_parse
{
    public $xml_massiv;

    /**
     * @var array
     */
    public $rezultat_obrabotki;
    /**
     * @var string
     */
    public $xml_filename = '../../data/dbf/DH142.DBF';

    public function __construct ()
    {
        //if (!empty($xml_text))
            //$this->xml_text = $xml_text;
        //elseif (empty($this->xml_text))
        $this->xml_massiv = $this->prochitat_xml_iz_fayla();
        var_dump(count($this->rezultat_obrabotki));
        die;
        $this->xml_v_massiv();
        //$this->rezultat_obrabotki = $this->ubiraem_iz_massiva_lishnee();
        //var_dump($this->rezultat_obrabotki);
        //return ($this->rezultat_obrabotki);
        $this->podschet_dannih_zvonkov();
    }

    private function xml_v_massiv ()
    {
        foreach ($this->xml_massiv as $line_num => $line)
            $this->rezultat_obrabotki[] = explode ('"', $line);
    }

    public function prochitat_xml_iz_fayla()
    {
        //читаем данные из файла
        if (file_exists($this->xml_filename) and is_readable($this->xml_filename))
        {
            $dbf = dbase_open($this->xml_filename, 0);

            $skolko_zapisey = dbase_numrecords($dbf);
            $count = 0;
            while ($count <= $skolko_zapisey)
            {
                $count ++;
                $massiv_s_dannimi[] = dbase_get_record_with_names($dbf, $count);
            }
        }
        else
        {
            print "File " . $this->xml_filename . " ne nayden-ne mogu prochest ishodnie dannie ;-(";
            die;
        }
        //print "Sobrano " . count($massiv_s_dannimi) . " strok v massiv.\r\n";
        //var_dump($massiv_s_dannimi[14]);
        return $massiv_s_dannimi;
    }

    private function podschet_dannih_zvonkov()
    {
        //считаем данные по звонкам - количество входящих, исходящих
        //и продолжительность входящих и исходящих

        $period = $this->opredelayem_time_proshlogo_mesyaca();

        $count = 0;
        foreach ($this->rezultat_obrabotki as $stroka_1 => $stroka)
        {
            $count ++;
            print "count = " . $count . " , time=" . $stroka['time'] . "\r\n";
            //проверяем, попадает ли дата в период прошлого месяца
            if ((int) $stroka['time'] < $period['nachalo_proshlogo_mesyaca'])
            {
                print "Vremya " . $stroka['time'] . " ne prodoshlo po periodu - menshe " . $period['nachalo_proshlogo_mesyaca'] . " -propuskaem.\r\n";
                continue;
            }
            if (((int) $stroka['time'] > $period['nachalo_etogo_mesyaca']))
            {
                print "Vremya " . $stroka['time'] . " ne prodoshlo po periodu - bolshe " . $period['nachalo_etogo_mesyaca'] . " -propuskaem.\r\n";
                continue;
            }
        }
    }

    private function opredelayem_time_proshlogo_mesyaca()
    {
        $period['nachalo_proshlogo_mesyaca'] = strtotime(date("d.m.Y",strtotime("first day of previous month"))); // первый день прошлого месяца
        $period['nachalo_etogo_mesyaca'] = strtotime(date("d.m.Y",strtotime("first day of this month")));
        //date("d.m.Y",strtotime("last day of previous month"));  // последний день прошлого месяца
        //TODO определяем месяц, за который надо посчтить звонки-на 1 меньше текущего
        var_dump($period);
        //sleep(100);
        return($period);
    }
}

$dbf = new dbf_1c77_parse();